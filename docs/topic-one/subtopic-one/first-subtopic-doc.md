---
title: First subtopic doc title
description: First subtopic doc description
---

# This is my first subtopical doc

It has some content in it.

- [Link to this subtopic index (folder, not readme)](./)
- [Link to this subtopic index (readme)](README.md)
- [Link to parent topic index (folder, not readme)](../)
- [Link to parent topic index (readme)](../README.md)
